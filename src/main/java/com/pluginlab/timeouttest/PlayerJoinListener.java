package com.pluginlab.timeouttest;

import io.netty.channel.ChannelDuplexHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelPromise;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class PlayerJoinListener implements Listener {

    static List<UUID> ignored = new ArrayList<>();

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {

        ((CraftPlayer) event.getPlayer()).getHandle().playerConnection.networkManager.channel
                .pipeline().addBefore("packet_handler", "PacketInjector2",
                        new TimeoutHandler(event.getPlayer().getUniqueId()));

    }

    private static class TimeoutHandler extends ChannelDuplexHandler {

        private final UUID playerId;

        private TimeoutHandler(UUID playerId) {
            this.playerId = playerId;
        }

        @Override
        public void write(ChannelHandlerContext chc, Object o, ChannelPromise cp) throws Exception {

            if (ignored.contains(playerId)) {
                return;
            }

            super.write(chc, o, cp);
        }

        @Override
        public void channelRead(ChannelHandlerContext chc, Object o) throws Exception {

            if (ignored.contains(playerId)) {
                return;
            }

            super.channelRead(chc, o);
        }

        @Override
        public void read(ChannelHandlerContext chc) throws Exception {

            if (ignored.contains(playerId)) {
                return;
            }

            super.read(chc);
        }

    }
}
