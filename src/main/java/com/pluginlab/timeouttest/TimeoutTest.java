package com.pluginlab.timeouttest;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public class TimeoutTest extends JavaPlugin {

    @Override
    public void onEnable() {

        Bukkit.getPluginManager().registerEvents(new PlayerJoinListener(), this);
        getCommand("timeout").setExecutor(new TimeoutCommand());

    }

}
